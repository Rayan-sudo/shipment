import React from 'react';
import { Router, Route, Switch } from "react-router-dom";
import history from "./history";
import Login from './pages/login/login.js';
import Shipment from './pages/shipment/shipment.js';
import Register from './pages/register/register.js';
import AddShipment from './pages/addShipment/addShipment.js';
import EditShipment from './pages/editShipment/editShipment.js'
function App() {
  return (
    <div>
    <Router history={history}>
      <Switch>
         <Route path="/Login" component={Login}/>
         <Route path="/Register" component={Register}/>
         <Route path="/Shipment" component={Shipment}/>
         <Route path="/AddShipment" component={AddShipment}/>
         <Route path="/EditShipment/:id" component={EditShipment}/>
    </Switch>
</Router>

  </div>
  );
}

export default App;
