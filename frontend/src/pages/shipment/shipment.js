import React,{useState} from 'react';
import axios from '../../config/axios';
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';
 import classes from './shipment.modul.css';
import { Button } from '@material-ui/core';
import history from '../../history';


export default function Shipment(props) {


 axios.get('http://127.0.0.1:8000/api/shipment')
                .then(response => {
                  if (response){
                  console.log(response);
                  setData(response.data.data);
                  }
                });
                
   const [data, setData] = useState([]);

   function deleteShipment(id){
    console.log(id);
    axios.delete('http://127.0.0.1:8000/api/shipment/delete/' + id)
                .then(response => {
                   console.log(response);
                  window.location.reload();
                });
               
  }
function logout(){
  localStorage.removeItem('token');
  history.push("/login");
}

  return (<div>
    <button onClick={()=>logout()}>Logout</button>
   <h1> SHIPMENTS </h1>
    <Table>
      <Thead>
        <Tr>
          <Th>Waybill</Th>
          <Th>Costumer address</Th>
          <Th>Costumer name</Th>
          <Th>Costumer Phone number</Th>
        </Tr>
      </Thead>
      <Tbody>
    {  data.map((shipment)=>
      <Tr>
          <Td>{shipment.waybill}</Td>
          <Td>{shipment.c_address}</Td>
          <Td>{shipment.c_name}</Td>
          <Td>{shipment.c_phone}</Td>
          <Td><a href={"/editShipment/"+shipment.id}>Edit</a><br></br>
          <button onClick={()=>deleteShipment(shipment.id)}>Delete</button>
          </Td>
        </Tr>
    )}
      </Tbody>
    </Table>
    <a href="/AddShipment"><Button>Add Shipment</Button></a>
    </div>
  );
}