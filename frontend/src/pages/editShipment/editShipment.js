import React ,{useState,useEffect} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from '../../config/axios.js';
import history from '../../history';
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
      RJ
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));


export default function EditShipment(props) {
  const classes = useStyles();
  console.log(props.match.params.id);
  let [c_address,setAddress]=useState('');
  let [c_name,setName]=useState('');
  let [waybill,setWaybill]=useState('');
  let [c_phone,setPhone]=useState('');
  

  useEffect(() => {
    console.log("hellooo")
    axios.get('http://127.0.0.1:8000/api/shipment/show/'+props.match.params.id)
    .then(response => {
    
       setAddress(response.data.data.c_address);
       setName(response.data.data.c_name);
       setWaybill(response.data.data.waybill);
       setPhone(response.data.data.c_phone);
       
    });
  }, [])




  return (
      
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5"  >
          Edit shipment
        </Typography>
        <form className={classes.form} noValidate   onSubmit={
          (event) => {
            event.preventDefault();
           console.log ("slkdhasjkdh")
           console.log(props.match.params.id);
            axios.put('http://127.0.0.1:8000/api/shipment/update/'+props.match.params.id, {
              waybill:waybill,
              c_name:c_name,
              c_address:c_address,
              c_phone:c_phone
            })
            .then(response => {
              console.log(response);
             history.push('/Shipment')
              
            })
          }

        }  >
          <TextField
            onChange={(event)=>{
              setWaybill(event.target.value)
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="waybill"
            label="waybill"
            name="waybill"
            value={waybill}
            autoComplete="waybill"
            autoFocus
          />
          <TextField
          onChange={(event)=>{
            setAddress(event.target.value)
          }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="c_address"
            label="address"
            type="c_address"
            id="c_address"
            value={c_address}
            autoComplete="current-address"
          />
          <TextField
          onChange={(event)=>{
            setName(event.target.value)
          }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="c_name"
            label="name"
            type="c_name"
            id="c_name"
            value={c_name}
            autoComplete="current-name"
          />
          <TextField
          onChange={(event)=>{
            setPhone(event.target.value)
          }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="c_phone"
            label="phone"
            type="c_phone"
            id="c_phone"
            value={c_phone}
            autoComplete="current-phone"
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          
          >
            Edit
          </Button>
       
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}