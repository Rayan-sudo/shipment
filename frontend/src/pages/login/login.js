import React ,{useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import axios from '../../config/axios';
import Link from '@material-ui/core/Link';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import history from '../../history.js'
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
      RJ
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn(props) {
  const classes = useStyles();
  const [email,setEmail]=useState('');
  const [pass,setPass]=useState('');
  const url = window.$url;



  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <VpnKeyIcon/>
        </Avatar>
        <Typography component="h1" variant="h5"  >
          Sign in
        </Typography>
        <form className={classes.form} noValidate   onSubmit={
          (event) => {
            event.preventDefault();
                const response = axios.post('http://127.0.0.1:8000/api/auth/login',
                  {  email:email,
                      password:pass})
                .then(response => {
                    console.log(response); 
                    if (response.status==200) 
                {
                    console.log("done")
                    localStorage.setItem('token', response.data.access_token) 
                    console.log(localStorage.getItem('token'))
                 
                   history.push('./Shipment')
                    
                } 
                else 
                {
                    console.log("error");
                    alert("Incorrect email or password");
                }
                   
                });
    
           
       }

        }  >
          <TextField
            onChange={(event)=>{
              setEmail(event.target.value)
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Username"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
          onChange={(event)=>{
            setPass(event.target.value)
          }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <a href="/Register"><Button>Register</Button></a>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}