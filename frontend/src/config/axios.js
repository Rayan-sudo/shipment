const axios = require('axios');
axios.create({
    baseURL: 'http://127.0.0.1:8000/api/',
    timeout: 1000,
    headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}
  });
  axios.interceptors.response.use(response => {
    // console.log( response)
    return response
  })

  export default axios;