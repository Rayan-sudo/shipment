<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function () {
Route::get( '/shipment', 'App\Http\Controllers\ShipmentController@index' );
Route::get( '/shipment/show/{id}', 'App\Http\Controllers\ShipmentController@show' );
Route::post( '/shipment/create', 'App\Http\Controllers\ShipmentController@store' );
Route::put( '/shipment/update/{id}', 'App\Http\Controllers\ShipmentController@update' );
Route::delete( '/shipment/delete/{id}', 'App\Http\Controllers\ShipmentController@destroy' );
});


Route::group([
    'prefix' => 'auth'

], function ($router) {
    Route::post('/register', 'App\Http\Controllers\AuthController@register');
    Route::post('/login', 'App\Http\Controllers\AuthController@login');
    Route::post('/logout', 'App\Http\Controllers\AuthController@logout');
});