<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $table = 'shipment';
    protected $fillable = ['id' ,'user_id', 'waybill' , 'c_address','c_name','c_phone'];
}
