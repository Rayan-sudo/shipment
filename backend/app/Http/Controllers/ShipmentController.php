<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shipment;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ShipmentController extends Controller
{
 
    /**
     *@return mixed 
     */
    /**
     *To show all shipments page in database  
     */
    public function index()
    {  
        // return response()->json([
        //     'success' => Auth::user()->id,
            
        // ]);
        
        $shipment = Shipment::where("user_id","=",Auth::user()->id)->get();

        return response()->json([
            'success' => Auth::user()->id,
            'data' => $shipment,
            
        ]);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * to show one shipment from database by ID
     */
    public function show($id)
    {
        $shipment = Shipment::find($id);

        if (!$shipment) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, shipment with id ' . $id . ' cannot be found.'
            ], 400);
        }


        return response()->json([
            'success' => true,
            'data' => $shipment
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException

     */
    /**
     *Create  shipment
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           
            'waybill' => 'required',
            'c_address' => 'required',
            'c_name' => 'required',
            'c_phone' => 'required',

        ]);

        $shipment = new Shipment();
        $shipment->waybill = $request->waybill;
        $shipment->c_address = $request->c_address;
        $shipment->c_name = $request->c_name;
        $shipment->c_phone= $request->c_phone;
        $shipment->user_id=Auth::user()->id;

        if ($shipment->save())
            return response()->json([
                'success' => true,
                'data' => $shipment
                
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'shipment store function error'
            ], 500);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
   
     */
    /**
     * Update one shipment 
     */

    public function update(Request $request, $id)
    {
        $shipment = Shipment::find($id);

        if (!$shipment) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, shipment id:' . $id . 'cannot be found'
            ], 400);
        }

    
        $updated = $shipment->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                'success' => true,
                'data' => $shipment
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, shipment is not updated'
            ], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * to delete one shipment
     */
    public function destroy($id)
    {
        $shipment = Shipment::find($id);

        if (!$shipment) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, shipment id:' . $id . ' can not be found.'
            ], 400);
        }

        if ($shipment->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'you can not delete shipment'
            ], 500);
        }
    }
  

}
